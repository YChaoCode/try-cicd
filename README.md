# SampleCodes4CICD

## 在 ubuntu 環境安裝 gitlab runner
```bash
$> curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

$> sudo apt-get install gitlab-runner -y
```

## 註冊 runner 指令(Ubuntu 環境)
```bash
sudo gitlab-runner register -n --url https://gitlab.com/ --registration-token [剛取得的token] --executor docker --docker-image "docker:20.10.16" --docker-privileged --docker-volumes "/certs/client"
```